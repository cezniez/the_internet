import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class TestABTesting:

    @pytest.mark.usefixtures("driver")
    def test_ab_testingtitle(self, driver):
        wait = WebDriverWait(driver, 10)
        driver.get('https://the-internet.herokuapp.com/')
        wait.until(EC.presence_of_element_located((By.LINK_TEXT, 'A/B Testing')))
        driver.find_element(By.LINK_TEXT, 'A/B Testing').click()
        wait.until(EC.visibility_of_element_located((By.TAG_NAME, 'h3')))
        page_title = driver.find_element(By.TAG_NAME, 'h3').text
        print(page_title)
        assert page_title.istitle()
    @pytest.mark.usefixtures("driver")
    def test_ab_testing(self, driver):
        wait = WebDriverWait(driver, 10)
        driver.get('https://the-internet.herokuapp.com/')
        wait.until(EC.presence_of_element_located((By.LINK_TEXT, 'A/B Testing')))
        driver.find_element(By.LINK_TEXT, 'A/B Testing').click()
        wait.until(EC.visibility_of_element_located((By.TAG_NAME, 'h3')))
        page_title = driver.find_element(By.TAG_NAME, 'h3').text
        print(page_title)
        assert page_title in ("A/B Test Variation 1", "A/B Test Control")


