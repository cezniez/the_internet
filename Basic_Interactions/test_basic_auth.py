import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

class TestBasicAuth:

    @pytest.mark.usefixtures("driver")
    def test_basic_auth(self, driver):
        wait = WebDriverWait(driver, 10)
        driver.get('https://the-internet.herokuapp.com/')
        wait.until(EC.presence_of_element_located((By.LINK_TEXT, 'Basic Auth')))
        driver.get('http://admin:admin@the-internet.herokuapp.com/basic_auth')
        page_message = driver.find_element(By.TAG_NAME, 'p').text
        print(f"\n{page_message}")
        assert page_message in "Congratulations! You must have the proper credentials."
        time.sleep(2)

