import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class TestAddRemoveElements:

    @pytest.mark.usefixtures("driver")
    def test_ab_testingtitle(self, driver):
        wait = WebDriverWait(driver, 10)
        driver.get('https://the-internet.herokuapp.com/')
        wait.until(EC.presence_of_element_located((By.LINK_TEXT, 'Add/Remove Elements')))
        driver.find_element(By.LINK_TEXT, 'Add/Remove Elements').click()
        wait.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="content"]/div/button')))
        add_button = driver.find_element(By.XPATH, '//*[@id="content"]/div/button')
        add_button_text = driver.find_element(By.XPATH, '//*[@id="content"]/div/button').text
        print(f"\nButton text: {add_button_text}")
        assert add_button_text in "Add Element"
        add_button.click()
        wait.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="elements"]/button')))
        remove_button = driver.find_element(By.XPATH, '//*[@id="elements"]/button')
        remove_button_text = remove_button.text
        print(f"\nButton text: {remove_button_text}")
        assert remove_button.text in "Delete"

        remove_button.click()
        buttons = driver.find_elements(By.TAG_NAME, 'button')
        print(f"\nNumber of buttons: {len(buttons)}")
        assert len(buttons) is 1




#        try:
#            wait.until(EC.invisibility_of_element_located((By.XPATH, '//*[@id="elements"]/button'))
#        except TimeoutException:
#            assert False, "The Remove button should have disappeared."




