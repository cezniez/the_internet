import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By


class Checkboxes(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()

    def tearDown(self):
        self.driver.quit()

    def test_list_values_for_different_approaches(self):
        driver = self.driver
        driver.get('http://the-internet.herokuapp.com/checkboxes')
        checkboxes = driver.find_elements(By.CSS_SELECTOR, 'input[type="checkbox"]')

        print("With .get_attribute('checked')")            #get NONE or True
        for checkbox in checkboxes:
            print(checkbox.get_attribute('checked'))

        print("\nWith .is_selected")                        # get false or true
        for checkbox in checkboxes:
            print(checkbox.is_selected())

    def test_list_values_for_different_approachess(self):
        driver = self.driver
        driver.get('http://the-internet.herokuapp.com/checkboxes')
        checkboxes = driver.find_elements(By.CSS_SELECTOR, 'input[type="checkbox"]')

        assert checkboxes[1].get_attribute('checked')
        # or
        assert checkboxes[1].is_selected()

if __name__ == "__main__":
    unittest.main()