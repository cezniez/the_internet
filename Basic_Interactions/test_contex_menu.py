import pytest
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains

class TestContexMenu:

    @pytest.mark.usefixtures("driver")
    def test_contex_menu(self, driver):
        wait = WebDriverWait(driver, 10)
        driver.get('https://the-internet.herokuapp.com/context_menu')
        wait.until(EC.presence_of_element_located((By.ID, 'hot-spot')))
        element = driver.find_element(By.ID, 'hot-spot')
        ActionChains(driver).context_click(element).perform()
        alert = driver.switch_to.alert
        alert_text = alert.text
        print(f'\nAlert message: {alert_text}')
        assert "You selected a context menu" in alert_text
        alert.accept()
        time.sleep(2)




