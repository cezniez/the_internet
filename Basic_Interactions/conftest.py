import pytest
from selenium import webdriver
@pytest.fixture
def driver():
    print("Setting up the WebDriver...")
    driver = webdriver.Chrome()
    yield driver
    driver.quit()
    print("WebDriver shutting down")

@pytest.fixture(autouse=True)
def print_test_name(request):
    test_name = request.node.name
    print(f"\nRunning test: {test_name}")