import pytest
import requests
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

class TestBrokenImages:

    @pytest.mark.usefixtures("driver")
    def test_broken_images(self, driver):
        wait = WebDriverWait(driver, 10)
        driver.get('https://the-internet.herokuapp.com/broken_images')
        wait.until(EC.presence_of_element_located((By.TAG_NAME, 'img')))
        images = driver.find_elements(By.TAG_NAME, 'img')
        broken_images = []
        for image in images:
            src = image.get_attribute('src')
            response = requests.head(src)
            if response.status_code != 200:
                broken_images.append(src)
        time.sleep(2)

        assert not broken_images, f"Broken images found: {broken_images}"


