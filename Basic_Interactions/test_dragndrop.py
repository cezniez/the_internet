import pytest
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains

class TestDragAndDrop:

    @pytest.mark.usefixtures("driver")
    def test_dragdrop(self, driver):
        wait = WebDriverWait(driver, 10)
        driver.get('https://the-internet.herokuapp.com/drag_and_drop')
        wait.until(EC.presence_of_element_located((By.ID, 'column-a')))

        source_element = driver.find_element(By.ID, 'column-a')
        target_element = driver.find_element(By.ID, 'column-b')

        ActionChains(driver).drag_and_drop(source_element, target_element).perform()
        time.sleep(2)

        column_B = driver.find_element(By.ID, 'column-b')
        column_B_text = column_B.text
        print(f'\n Text in col b: {column_B_text}')

        assert "A" in column_B_text




